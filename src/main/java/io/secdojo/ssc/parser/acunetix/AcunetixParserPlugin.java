package io.secdojo.ssc.parser.acunetix;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fortify.plugin.api.ScanBuilder;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.ScanParsingException;
import com.fortify.plugin.api.VulnerabilityHandler;
import com.fortify.plugin.spi.ParserPlugin;
import io.secdojo.ssc.parser.acunetix.parser.ScanParser;
import io.secdojo.ssc.parser.acunetix.parser.VulnerabilitiesParser;

public class AcunetixParserPlugin implements ParserPlugin<CustomVulnAttribute> {
    private static final Logger LOG = LoggerFactory.getLogger(AcunetixParserPlugin.class);
    private String url;
    @Override
    public void start() throws Exception {
        LOG.info("{} is starting", this.getClass().getSimpleName());
    }

    @Override
    public void stop() throws Exception {
        LOG.info("{} is stopping", this.getClass().getSimpleName());
    }

    @Override
    public Class<CustomVulnAttribute> getVulnerabilityAttributesClass() {
        return CustomVulnAttribute.class;
    }

    @Override
    public void parseScan(final ScanData scanData, final ScanBuilder scanBuilder) throws ScanParsingException, IOException {
        ScanParser scanParser = new ScanParser(scanData, scanBuilder);
        url = scanParser.parse();
    }

	@Override
	public void parseVulnerabilities(final ScanData scanData, final VulnerabilityHandler vulnerabilityHandler) throws ScanParsingException, IOException {
        VulnerabilitiesParser vulnerabilitiesParser = new VulnerabilitiesParser(scanData, vulnerabilityHandler);
        vulnerabilitiesParser.parse(url);
	}
}
