package io.secdojo.ssc.parser.acunetix.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TechnicalDetails {
    @JsonProperty private String Request;
}
