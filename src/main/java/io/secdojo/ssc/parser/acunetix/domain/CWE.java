package io.secdojo.ssc.parser.acunetix.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CWE {
    @JsonProperty
    private String CWE = new String();
}
