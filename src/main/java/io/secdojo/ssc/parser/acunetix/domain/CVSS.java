package io.secdojo.ssc.parser.acunetix.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CVSS {
    @JsonProperty private String Descriptor = new String();
    @JsonProperty private String Score = new String();
}
