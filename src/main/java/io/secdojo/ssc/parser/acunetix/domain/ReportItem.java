/*******************************************************************************
 * (c) Copyright 2020 Micro Focus or one of its affiliates
 *
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the 
 * "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to 
 * whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY 
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE 
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 ******************************************************************************/
package io.secdojo.ssc.parser.acunetix.domain;

import java.util.ArrayList;
//import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.Data;

@Data
public class ReportItem {
    @JsonProperty private String Name;
    @JsonProperty private String ModuleName;
    @JsonProperty private String Details;
    @JsonProperty private String Affects;
	@JsonProperty private String Parameter;
	@JsonProperty private String Severity;
	@JsonProperty private String Type;
	@JsonProperty private String Impact;
	@JsonProperty private String Description;
	@JsonProperty private String DetailedInformation;
	@JsonProperty private String Recommendation;
	@JsonProperty private String IsFalsePositive;
	
	@JsonProperty private TechnicalDetails TechnicalDetails;

	//@JacksonXmlElementWrapper(localName = "CWEList")
	//public final List<CWE> CWEList = new ArrayList<CWE>();
	
	@JacksonXmlElementWrapper(localName = "References")
	@JacksonXmlProperty(localName = "Reference")
	public final List<Reference> references = new ArrayList<Reference>();
	
	@JsonProperty
	@JacksonXmlProperty(localName = "CVSS")
	@JacksonXmlElementWrapper(useWrapping = false)
	private CVSS CVSS;
	
	@JsonProperty
	@JacksonXmlProperty(localName = "CVSS3")
	@JacksonXmlElementWrapper(useWrapping = false)
	private CVSS CVSS3 = new CVSS();
	
}