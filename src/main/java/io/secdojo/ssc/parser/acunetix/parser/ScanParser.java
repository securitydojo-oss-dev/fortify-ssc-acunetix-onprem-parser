package io.secdojo.ssc.parser.acunetix.parser;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fortify.plugin.api.ScanBuilder;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.ScanParsingException;
import com.fortify.util.ssc.parser.xml.ScanDataStreamingXmlParser;
import com.fortify.util.xml.ExtendedXMLStreamReader;

public class ScanParser {
	private static final Logger LOG = LoggerFactory.getLogger(ScanParser.class);
	private final ScanData scanData;
	private final ScanBuilder scanBuilder;
	private String url;
    
	public ScanParser(final ScanData scanData, final ScanBuilder scanBuilder) {
		this.scanData = scanData;
		this.scanBuilder = scanBuilder;
	}
	
	public final String parse() throws ScanParsingException, IOException {
		new ScanDataStreamingXmlParser()
			.handler("/ScanGroup", this::processScanGroupAttributes)
			.parse(scanData);
		scanBuilder.completeScan();
		return this.url;
	}
	
	private final void processScanGroupAttributes(ExtendedXMLStreamReader xsr) {
		scanBuilder.setScanDate(parseScanDate(xsr.getAttributeValue(null, "ExportedOn")));
		this.url = xsr.getAttributeValue(null, "ScanUrl");
		scanBuilder.setEngineVersion("Acunetix Version: 13.0.200217097");
		xsr.setSkipRemaining(true);
	}
	
	private final Date parseScanDate(String dateString) {
		Date date;
		try {
			date = new SimpleDateFormat("dd/mm/yyyy, H:m:s").parse(dateString);
		} catch (ParseException e) {
			LOG.warn(String.format("Error parsing {} as date", dateString), e);
			date = new Date();
		}
		return date;
	}
}
