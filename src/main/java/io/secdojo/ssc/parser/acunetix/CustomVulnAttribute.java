package io.secdojo.ssc.parser.acunetix;

public enum CustomVulnAttribute implements com.fortify.plugin.spi.VulnerabilityAttribute {
    
    vulnerability(AttrType.LONG_STRING),
    isFalsePositive(AttrType.STRING),
    severity(AttrType.STRING),
	path(AttrType.STRING),
	recommendation(AttrType.LONG_STRING),
	references(AttrType.LONG_STRING),
    request(AttrType.LONG_STRING),
    type(AttrType.STRING),
    CVSS(AttrType.STRING),
    CVSS3(AttrType.STRING),
    CWE(AttrType.STRING),
    url(AttrType.STRING),
	;

    private final AttrType attributeType;

    CustomVulnAttribute(final AttrType attributeType) {
        this.attributeType = attributeType;
    }

    @Override
    public String attributeName() {
        return name();
    }

    @Override
    public AttrType attributeType() {
        return attributeType;
    }
}
