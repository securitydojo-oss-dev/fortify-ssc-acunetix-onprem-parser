package io.secdojo.ssc.parser.acunetix.parser;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fortify.plugin.api.BasicVulnerabilityBuilder.Priority;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.ScanParsingException;
import com.fortify.plugin.api.StaticVulnerabilityBuilder;
import com.fortify.plugin.api.VulnerabilityHandler;
import io.secdojo.ssc.parser.acunetix.CustomVulnAttribute;
import io.secdojo.ssc.parser.acunetix.domain.CVSS;
import io.secdojo.ssc.parser.acunetix.domain.Reference;
import io.secdojo.ssc.parser.acunetix.domain.ReportItem;
import com.fortify.util.ssc.parser.EngineTypeHelper;
import com.fortify.util.ssc.parser.xml.ScanDataStreamingXmlParser;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

public class VulnerabilitiesParser {
	private static final String ENGINE_TYPE = EngineTypeHelper.getEngineType();
	private static final Map<String, Priority> MAP_SEVERITY_TO_PRIORITY = Stream.of(
			  new AbstractMap.SimpleImmutableEntry<>("information", Priority.Low),    
			  new AbstractMap.SimpleImmutableEntry<>("low", Priority.Medium),
			  new AbstractMap.SimpleImmutableEntry<>("medium",Priority.High),
              new AbstractMap.SimpleImmutableEntry<>("high",Priority.Critical))
		.collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
	private final ScanData scanData;
	private final VulnerabilityHandler vulnerabilityHandler;
	private String scanUrl;
	//private static int id = 0;
	

    public VulnerabilitiesParser(final ScanData scanData, final VulnerabilityHandler vulnerabilityHandler) {
    	this.scanData = scanData;
		this.vulnerabilityHandler = vulnerabilityHandler;
	}
    
    /**
	 * Main method to commence parsing the input provided by the configured {@link ScanData}.
	 * @throws ScanParsingException
	 * @throws IOException
	 */
	public final void parse(String scanUrl) throws ScanParsingException, IOException {
		this.scanUrl = scanUrl;
		new ScanDataStreamingXmlParser()
			.handler("/ScanGroup/Scan/ReportItems/ReportItem", ReportItem.class, this::buildVulnerabilities)
			.parse(scanData);
	}
	
	
	private final void buildVulnerabilities(ReportItem reportItem) {
		StaticVulnerabilityBuilder vb = vulnerabilityHandler.startStaticVulnerability(getInstanceId(reportItem));
		vb.setEngineType(ENGINE_TYPE);
		//vb.setKingdom(FortifyKingdom.ENVIRONMENT.getKingdomName());
		vb.setAnalyzer("Acunetix");
		vb.setCategory(reportItem.getName());
		//vb.setCategory("test categorie");
		// Set mandatory values to JavaDoc-recommended values
		vb.setAccuracy(5.0f);
		vb.setLikelihood(2.5f);
		vb.setPriority(getPriority(reportItem));	
		vb.setStringCustomAttributeValue(CustomVulnAttribute.severity, reportItem.getSeverity());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.path, reportItem.getAffects());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.isFalsePositive, StringUtils.defaultIfEmpty(reportItem.getIsFalsePositive(),"false"));
		vb.setStringCustomAttributeValue(CustomVulnAttribute.vulnerability, getDetailsText(reportItem));
		vb.setStringCustomAttributeValue(CustomVulnAttribute.recommendation, reportItem.getRecommendation());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.type, reportItem.getType());
		vb.setStringCustomAttributeValue(CustomVulnAttribute.references, getAllReferences(reportItem));
		vb.setStringCustomAttributeValue(CustomVulnAttribute.request, getRequestAsHtml(reportItem));
		vb.setStringCustomAttributeValue(CustomVulnAttribute.url, this.scanUrl);
		vb.setStringCustomAttributeValue(CustomVulnAttribute.CVSS, getDataCVSS(reportItem.getCVSS()));
		vb.setStringCustomAttributeValue(CustomVulnAttribute.CVSS3, getDataCVSS(reportItem.getCVSS3()));
		//vb.setStringCustomAttributeValue(CustomVulnAttribute.CWE, getCWE(reportItem.getCWEList()));

		vb.completeVulnerability();
    }

	private String getInstanceId(ReportItem reportItem) {
		
		StringBuilder sb = new StringBuilder();
		sb.append(reportItem.getName());
		//sb.append(reportItem.getCWEList().getCWE());
		sb.append(reportItem.getTechnicalDetails().getRequest());
		sb.append(reportItem.getAffects());
		String sha256hex = DigestUtils.sha256Hex(sb.toString());
		return sha256hex;
		//return Integer.toString(++id);
	}

	
	private Priority getPriority(ReportItem reportItem) {
		return MAP_SEVERITY_TO_PRIORITY.getOrDefault(reportItem.getSeverity(), Priority.Medium);
	}
	
	private String getDetailsText(ReportItem reportItem){
		StringBuilder sb = new StringBuilder();
		appendSection(sb, "Description", reportItem.getDescription());
		sb.append("<br/>");
		appendSection(sb, "Impact", reportItem.getImpact());
		sb.append("<br/>");
		appendSection(sb, "Detailed Information", reportItem.getDetailedInformation());
		sb.append("<br/>");
		appendSection(sb, "Details", reportItem.getDetails());
		sb.append("<br/>");
		appendSection(sb, "Recommendation", reportItem.getRecommendation());
		return sb.toString();
	}
	
	private final void appendSection(StringBuilder sb, String header, String text) {
		if ( StringUtils.isNotBlank(text) ) {
			sb.append("<b>").append(header).append("</b><br/><br/>\n").append(text).append("<br/>\n");
		}
	}

	private final void appendSectionInLine(StringBuilder sb, String header, String text) {
		if ( StringUtils.isNotBlank(text) ) {
			sb.append("<b>").append(header).append("</b>\n").append(text).append("<br/>\n");
		}
	}
	
	private String getAllReferences(ReportItem reportItem){
		StringBuilder sb = new StringBuilder();
		for (Reference reference : reportItem.getReferences()) {
			appendSectionInLine(sb, "Database", reference.getDatabase());
			appendSectionInLine(sb, "Url", reference.getURL());
			sb.append("<br/>");
		}
		return sb.toString();

	}

	private String getRequestAsHtml(ReportItem reportItem){
		StringBuilder sb = new StringBuilder();
		getCodeAsHtml(sb, reportItem.getTechnicalDetails().getRequest());
		sb.append("<br/>");
		return sb.toString();
	}

	private final void getCodeAsHtml(StringBuilder sb, String code) {
		if ( StringUtils.isNotBlank(code) ) {
			final String codePrefix = "<pre><code>";
			final String codeSuffix = "</code></pre>";			
			sb.append(codePrefix)
				.append(code)
				.append(codeSuffix);
		} 
	}

	private String getDataCVSS(CVSS cvss){
		StringBuilder sb = new StringBuilder();
		sb.append("Score: ")
			.append(cvss.getScore())
			.append(" Descriptor: ")
			.append(cvss.getDescriptor());
		return sb.toString();
	}

	
	/*
	private String getCWE(ReportItem reportItem){
		StringBuilder sb = new StringBuilder();
		for (CWEList cwe : reportItem.getCWEList()) {
			appendSectionInLine(sb, "CWE", cwe.getCWE());
			sb.append("<br/>");
		}
		return sb.toString();
	}
	*/
	
}
